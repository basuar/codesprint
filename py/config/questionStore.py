questionDataConfiguration = {
    'q1' : {
        'title':        'The sum of the parts',
        'description':  'You\'re given a list of integers.<br>' +\
                        'You must find their cumulative sum with full precision.<br>' +\
                        '', 
        'inputFormat':  'The first line has an integer <b>T</b> - the number of test-cases.<br>' + \
                        'T lines follow, each describing one test-case.<br>' + \
                        'A single test-case contains a space separated list of positive integers.<br>' + \
                        '0 < number of positive integers in a test-case < 10<sup>3</sup>.<br>' + \
                        '0 < value of each positive integer in a test-case < 10<sup>18</sup>.<br>' + \
                        '',
        'outputFormat': 'For each test case, print a single line containing the sum of all the numbers in that test case.',
        'example':      'Sample Input:<br>' + \
                        '2<br>' + \
                        '1 2 3<br>' + \
                        '10 20 30<br>' + \
                        '<br>' + \
                        'Sample Output:<br>' + \
                        '6<br>' + \
                        '60<br>' + \
                        '<br>' +\
                        'Explanation:<br>' +\
                        '1 + 2 + 3 = 6<br>' +\
                        '10 + 20 + 30 = 60<br>' +\
                        '',
    },

    'q2' : {
        'title':        'The sum of the parts...2',
        'description':  'You\'re given a list of integers.<br>' +\
                        'You must find their cumulative sum with full precision.' +\
                        '<i>that\'s it ;)</i>',
        'inputFormat':  'The first line has an integer <b>T</b> - the number of testcases.<br>' + \
                        'Each test case\'s input consists of a single line.<br>' + \
                        'This line contains a space separated list of integers (32 bit integers - upto 10<sup>5</sup> integers per line).<br>' + \
                        '',
        'outputFormat': 'For each test case, print a single line. In case the no solution exists, print a blank line.',
        'example':      'Sample Input:<br>' + \
                        '1<br>' + \
                        '1 2 3<br>' + \
                        '' + \
                        'Sample Output:<br>' + \
                        '6' + \
                        '<br>' +\
                        'Explanation:<br>' +\
                        '1 + 2 + 3 = 6<br>' +\
                        '',
    },

    'q3' : {
        'title':        'The sum of the parts...3',
        'description':  'You\'re given a list of integers.<br>' +\
                        'You must find their cumulative sum with full precision.' +\
                        '<i>that\'s it ;)</i>',
        'inputFormat':  'The first line has an integer <b>T</b> - the number of testcases.<br>' + \
                        'Each test case\'s input consists of a single line.<br>' + \
                        'This line contains a space separated list of integers (32 bit integers - upto 10<sup>5</sup> integers per line).<br>' + \
                        '',
        'outputFormat': 'For each test case, print a single line. In case the no solution exists, print a blank line.',
        'example':      'Sample Input:<br>' + \
                        '1<br>' + \
                        '1 2 3<br>' + \
                        '' + \
                        'Sample Output:<br>' + \
                        '6' + \
                        '',
    },

    'q4' : {
        'title':        'The sum of the parts...4',
        'description':  'You\'re given a list of integers.<br>' +\
                        'You must find their cumulative sum with full precision.' +\
                        '<i>that\'s it ;)</i>',
        'inputFormat':  'The first line has an integer <b>T</b> - the number of testcases.<br>' + \
                        'Each test case\'s input consists of a single line.<br>' + \
                        'This line contains a space separated list of integers (32 bit integers - upto 10<sup>5</sup> integers per line).<br>' + \
                        '',
        'outputFormat': 'For each test case, print a single line. In case the no solution exists, print a blank line.',
        'example':      'Sample Input:<br>' + \
                        '1<br>' + \
                        '1 2 3<br>' + \
                        '' + \
                        'Sample Output:<br>' + \
                        '6' + \
                        '',
    },

    'q5' : {
        'title':        'The sum of the parts...5',
        'description':  'You\'re given a list of integers.<br>' +\
                        'You must find their cumulative sum with full precision.' +\
                        '<i>that\'s it ;)</i>',
        'inputFormat':  'The first line has an integer <b>T</b> - the number of testcases.<br>' + \
                        'Each test case\'s input consists of a single line.<br>' + \
                        'This line contains a space separated list of integers (32 bit integers - upto 10<sup>5</sup> integers per line).<br>' + \
                        '',
        'outputFormat': 'For each test case, print a single line. In case the no solution exists, print a blank line.',
        'example':      'Sample Input:<br>' + \
                        '1<br>' + \
                        '1 2 3<br>' + \
                        '' + \
                        'Sample Output:<br>' + \
                        '6' + \
                        '',
    },

    'q6' : {
        'title':        'The sum of the parts...6',
        'description':  'You\'re given a list of integers.<br>' +\
                        'You must find their cumulative sum with full precision.' +\
                        '<i>that\'s it ;)</i>',
        'inputFormat':  'The first line has an integer <b>T</b> - the number of testcases.<br>' + \
                        'Each test case\'s input consists of a single line.<br>' + \
                        'This line contains a space separated list of integers (32 bit integers - upto 10<sup>5</sup> integers per line).<br>' + \
                        '',
        'outputFormat': 'For each test case, print a single line. In case the no solution exists, print a blank line.',
        'example':      'Sample Input:<br>' + \
                        '1<br>' + \
                        '1 2 3<br>' + \
                        '' + \
                        'Sample Output:<br>' + \
                        '6' + \
                        '',
    },
}
