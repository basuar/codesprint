import time
import logging
import templateDataManager
from configurationManager import questionDataConfiguration
from httpRequestHandlers import BaseRequestHandler

class QuestionStoreHandler(BaseRequestHandler):
    def get(self):
        logging.debug("In QuestionStoreHandler::get()...")

        templateData = templateDataManager.getTemplateData(self.request)

        currentTimeAsEpochTime = int(time.mktime(time.localtime()))
        templateData['currentTimeAsEpochTime'] = currentTimeAsEpochTime
        logging.debug('currentTimeAsEpochTime: [%s] contestStartTimeAsEpochTime: [%s] contestEndTimeAsEpochTime: [%s]' % \
                        (currentTimeAsEpochTime, templateData['contestStartTimeAsEpochTime'], templateData['contestEndTimeAsEpochTime']))

        questionString = ''

        questionIndex = int(self.request.get('qIdx'))
        if questionIndex is None:
            logging.critical('Did not receive a valid questionIndex [%s].' % request.get('qIdx'))
        elif currentTimeAsEpochTime < templateData['contestEndTimeAsEpochTime'] and currentTimeAsEpochTime > templateData['contestStartTimeAsEpochTime']:
            numberOfReleasedQuestions =  1 + int((currentTimeAsEpochTime - int(templateData['contestStartTimeAsEpochTime'])) / (60 * int(templateData['questionReleaseIntervalInMinutes'])))
            questionCode = 'q'+str(questionIndex)
            if questionCode in questionDataConfiguration and questionIndex >= 1 and questionIndex <= numberOfReleasedQuestions:
                questionString = str(questionDataConfiguration[questionCode])
                logging.debug('Sending question [%s]:\n%s' % (questionCode, questionString))
            else:
                logging.critical('Unable to send question [%s].' % questionCode)
        else:
            logging.critical('Received a request for question outside the contest\'s timings.')

        # Render the question
        self.response.write(questionString)

