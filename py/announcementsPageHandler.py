import logging
import templateDataManager 
from httpRequestHandlers import BaseRequestHandler

class AnnouncementsPageHandler(BaseRequestHandler):
    def get(self):
        logging.debug("In AnnouncementsPageHandler::get():\n%s", self.request)

        templateData = templateDataManager.getTemplateData(self.request)

        # Read the announcement file
        logging.debug('Reloading the announcements module')
        try:
            templateData['announcements'] = reload(__import__('config.announcements').announcements).announcements

        except Exception, e:
            logging.error('Encountered an error [%s] while trying to reload the announcements module' % e)

        # Render the page
        self.render_response('announcementsPage.html', **templateData)

