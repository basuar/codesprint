import logging
from httpRequestHandlers import BaseRequestHandler

class IndexPageHandler(BaseRequestHandler):
    def get(self):
        logging.debug("In IndexPageHandler::get()...")
        self.redirect('/home')

