import os
import ast
import errno
import inspect
import logging
import datetime
import config.teamData
import config.questionStore
from config.applicationConfiguration import applicationConfiguration

teamDataConfiguration = {}
questionDataConfiguration = {}

def initialize():
    # Initialize logging configuration
    logging.basicConfig(filename = '%s/../logs/codesprint.%s.log' % (os.path.dirname(inspect.getfile(inspect.currentframe())), datetime.datetime.now().date()),
                        format   = '%(asctime)s %(levelname)s: %(message)s',
                        level    = logging.DEBUG if applicationConfiguration['isDebugMode'] is True else logging.INFO)

    global questionDataConfiguration
    questionDataConfiguration = config.questionStore.questionDataConfiguration

    if (len(questionDataConfiguration) != applicationConfiguration['numberOfQuestions']):
        logging.critical('The number of available questions [%s] does NOT match the number of configured questions [%s].' %(len(questionDataConfiguration), pplicationConfiguration['numberOfQuestions']))

    global teamDataConfiguration
    teamDataConfiguration = config.teamData.teamDataConfiguration

    # Read the individual team's score files
    pathPrefix = os.path.dirname(inspect.getfile(inspect.currentframe())) + '/../teamData/'
    try:
        teamCode = ''
        for team in teamDataConfiguration:
            teamCode = team
            logging.debug('Reading the score (initial) for team [%s]' % team)
            path = pathPrefix + team + '.score'
            try:
                fh = open(path, 'r')
                teamDataConfiguration[team]['scoreData'] = ast.literal_eval(fh.read())
                fh.close()

            except IOError, e:
                if e.errno is not errno.ENOENT:
                    fh.close()
                    raise Exception(e)
                else:
                    teamDataConfiguration[team]['scoreData'] = {'points': 0, 'timeTaken': 0}
                    logging.debug('No score found for team [%s]' % team)

    except Exception, e:
        logging.critical('Encountered an error [%s] while trying to read the score for team [%s]' % (e, teamCode))

