import time
import logging
import templateDataManager
from httpRequestHandlers import BaseRequestHandler
from configurationManager import questionDataConfiguration, teamDataConfiguration

class QuestionsPageHandler(BaseRequestHandler):
    def get(self):
        logging.debug("In QuestionsPageHandler::get()...")

        templateData = templateDataManager.getTemplateData(self.request)

        currentTimeAsEpochTime = int(time.mktime(time.localtime()))
        templateData['currentTimeAsEpochTime'] = currentTimeAsEpochTime
        logging.debug('currentTimeAsEpochTime: [%s] contestStartTimeAsEpochTime: [%s] contestEndTimeAsEpochTime: [%s]' % \
                        (currentTimeAsEpochTime, templateData['contestStartTimeAsEpochTime'], templateData['contestEndTimeAsEpochTime']))

        numberOfReleasedQuestions = 0
        if currentTimeAsEpochTime >= templateData['contestEndTimeAsEpochTime']:
            numberOfReleasedQuestions = templateData['numberOfQuestions']
            templateData['timeToNextInterval'] = 0
        if currentTimeAsEpochTime > templateData['contestStartTimeAsEpochTime']:
            if numberOfReleasedQuestions is 0:
                numberOfReleasedQuestions =  1 + int((currentTimeAsEpochTime - int(templateData['contestStartTimeAsEpochTime'])) / (60 * int(templateData['questionReleaseIntervalInMinutes'])))
                templateData['timeToNextInterval'] = int(((numberOfReleasedQuestions * int(templateData['questionReleaseIntervalInMinutes'])) * 60) + \
                                                    templateData['contestStartTimeAsEpochTime'] - currentTimeAsEpochTime)
            questionData = {}
            for i in xrange(1, numberOfReleasedQuestions + 1):
                questionCode = 'q'+str(i)
                if questionCode in questionDataConfiguration:
                    questionData[questionCode] = questionDataConfiguration[questionCode]
            templateData['questionData'] = str(questionData)
            logging.debug('numberOfReleasedQuestions: [%s] timeToNextInterval: [%s]' % (numberOfReleasedQuestions, templateData['timeToNextInterval']))

        templateData['numberOfReleasedQuestions'] = numberOfReleasedQuestions

        currentTeamName = ''
        currentTeamPoints = 0
        currentTeamTimeTaken = 0
        if templateData['teamCode'] in teamDataConfiguration:
            currentTeamName = teamDataConfiguration[templateData['teamCode']]['teamName']
            logging.debug('Current teamName is [%s]' % currentTeamName)
            currentTeamPoints = teamDataConfiguration[templateData['teamCode']]['scoreData']['points']
            currentTeamTimeTaken = teamDataConfiguration[templateData['teamCode']]['scoreData']['timeTaken']

        templateData['currentTeamName'] = currentTeamName
        templateData['currentTeamPoints'] = currentTeamPoints
        templateData['currentTeamTimeTaken'] = currentTeamTimeTaken

        try:
            currentQuestionIndex = int(self.request.get('qIdx'), 0)
        except Exception, e:
            currentQuestionIndex = numberOfReleasedQuestions 
        templateData['currentQuestionIndex'] = currentQuestionIndex

        # Render the page
        self.render_response('questionsPage.html', **templateData)

