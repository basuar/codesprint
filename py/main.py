import logging

# Application's main function
def main():
    # Read application configuration
    import configurationManager
    from configurationManager import applicationConfiguration, teamDataConfiguration
    configurationManager.initialize()

    logging.info('Starting the [codesprint] server on host [%s]:[%s]' % (applicationConfiguration['hostName'], applicationConfiguration['serverPort']))
    logging.info('Initial applicationConfiguration:\n%s' % applicationConfiguration)
    logging.info('Initial teamDataConfiguration:\n%s' % teamDataConfiguration)

    import templateDataManager
    templateDataManager.initialize()

    # Prepare HTTP request routers
    import webapp2
    app = webapp2.WSGIApplication([ ('/', 'indexPageHandler.IndexPageHandler'),
                                    ('/home', 'homePageHandler.HomePageHandler'),
                                    ('/questions', 'questionsPageHandler.QuestionsPageHandler'),
                                    ('/getQuestion', 'questionStoreHandler.QuestionStoreHandler'),
                                    ('/leaderboard', 'leaderboardPageHandler.LeaderboardPageHandler'),
                                    ('/announcements', 'announcementsPageHandler.AnnouncementsPageHandler'),
                                    ('/help', 'helpPageHandler.HelpPageHandler'),
                                    (r'/questionStore/[0-9a-z]{6}\.[small|large]', 'httpRequestHandlers.StaticDataHandler'),
                                    (r'/.+[png|css|js]', 'httpRequestHandlers.StaticDataHandler'),
                                  ], debug = applicationConfiguration['isDebugMode'])

    # Run HTTP server.
    from paste import httpserver
    httpserver.serve(app, applicationConfiguration['hostName'], applicationConfiguration['serverPort'])

if __name__ == '__main__':
    main()
